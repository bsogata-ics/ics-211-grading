/*
 * Handles client-side activity for the ICS 211 Grade Calculations site.
 *
 * Author: Branden Ogata
 */
 
/*
 * Updates the final grade calculations.
 */
 
function updateGrade()
{
  var assignments = parseFloat($("#assignment-total").text().substring(0, $("#assignment-total").text().length));
  var lectureQuizzes = parseFloat($("#lecture-total").text().substring(0, $("#lecture-total").text().length));
  var labQuizzes = parseFloat($("#lab-total").text().substring(0, $("#lab-total").text().length));
  var midterms = parseFloat($("#midterm-total").text().substring(0, $("#midterm-total").text().length));
  
  var preFinalTotal = assignments + lectureQuizzes + labQuizzes + midterms;
  var targetButton = $("#grade-b");
  var targetGrade = 83;
  
  // Find the selected grade from the list of buttons
  $(".grade-button").each(function() 
  {
    if ($(this).hasClass("active"))
    {
      targetButton = $(this);
      targetGrade = parseInt($(this).text().substr($(this).text().indexOf("(") + 1, 2));
    }
  });
  
  $("#final-exam-grade").val((((targetGrade - preFinalTotal) / 20.0) * 100).toFixed(1) + "%");
  $("#final-grade").val($(targetButton).text());  
}
 
/*
 * Validates the given fields to ensure that their values are within the given ranges.
 *
 * Parameters:
 *   toValidate    The jQuery object containing the fields to validate.
 *   min           The integer equal to the lower bound of acceptable values.
 *   max           The integer equal to the upper bound of acceptable values.
 *
 * Returns:
 *   A boolean value that is true if all of the given fields are valid,
 *                           false otherwise.
 *
 */

function validateFields(toValidate, min, max)
{
  var allValid = true;
  
  $(toValidate).each(function()
  {
    var currentValue = $(this).val();
    
    // If not a number or not in the given range, invalidate
    if ((isNaN(currentValue)) || (parseFloat(currentValue) < min) || (max < parseFloat(currentValue)))
    {
      allValid = false;
      $(this).parent().addClass("has-error");
    }  
    // Else validate the field
    else
    {
      $(this).parent().removeClass("has-error");
    }
  });
  
  return allValid;
} 
 
$(document).ready(function()
{
  // If one of the assignment inputs changes, update the assignment total
  $(".assignment-input").change(function()
  {
    // If all fields are valid, update the assignment total
    if (validateFields($(".assignment-input"), 0, 10))
    {
      var sum = 0;
      var min = 16;
      
      // Add each assignment score
      $(".assignment-input").each(function() 
      {
        var current = (isNaN(parseFloat($(this).val()))) ? (0.0) : (parseFloat($(this).val()));
        sum += current;
        
        // Update min if necessary
        if (current < min)
        {
          min = current;
        }
      });
      
      // Remove the lowest assignment score
      sum -= min;
      
      $("#assignment-total").text(((sum * 10 * 0.30) / ($(".assignment-input").length - 1)).toFixed(1) + "%");
      $("#assignment-total").change();
    }
  });
  
  // If the lecture quiz input changes, update the in-class quiz total
  $(".lecture-input").change(function()
  {
    // If the field is valid, update
    if (validateFields($(".lecture-input"), 0, 100))
    {
      var score = (isNaN(parseFloat($(this).val()))) ? (0.0) : (parseFloat($(this).val()));
      $("#lecture-total").text((score * 0.15).toFixed(1) + "%");
      $("#lecture-total").change();
    }
  });
  
  // If the lab quiz input changes, update the lab quiz total
  $(".lab-input").change(function()
  {
    // If the field is valid, update
    if (validateFields($(".lab-input"), 0, 100))
    {
      var score = (isNaN(parseFloat($(this).val()))) ? (0.0) : (parseFloat($(this).val()));
      $("#lab-total").text((score * 0.15).toFixed(1) + "%");
      $("#lab-total").change();
    }
  });
  
  // If one of the midterm inputs changes, update the midterm total
  $(".midterm-input").change(function()
  {
    // If the fields are valid, update
    if (validateFields($(".midterm-input"), 0, 100))
    {
      var sum = 0;

      // Add each midterm score
      $(".midterm-input").each(function() 
      {
        sum += (isNaN(parseFloat($(this).val()))) ? (0.0) : (parseFloat($(this).val()));
      });
      
      $("#midterm-total").text(((sum * 0.20) / 2).toFixed(1) + "%");
      $("#midterm-total").change();
    }
  });
  
  // If any of the totals change, update the calculations for the final
  $(".badge").change(function()
  {
    updateGrade();
  });
  
  // If one of the grade buttons is selected, make the button active and update the grade calculations
  $(".grade-button").click(function()
  {
    $(".grade-button").removeClass("active");
    $(this).addClass("active");
    
    updateGrade();
  });
  
  // If one of the spinner buttons is clicked, increase or decrease the corresponding value if possible
  $(".spinner-button").click(function()
  {
    var group = $(this).parent().parent().parent();
    var field = $(group).find("input");
    var value = $(field).val();
  
    // If an up button, increase the value
    if ($(this).find(".glyphicon-chevron-up").size() > 0)
    {
      if (validateFields(field, 0, ($(field).hasClass("assignment-input")) ? (10) : (100)))
      {
        // Stop from increasing over the upper bound
        if (parseFloat(value) < (($(field).hasClass("assignment-input")) ? (10) : (100)))
        {
          console.log("Value: " + value);
          console.log("Field: " + ($(field).hasClass("assignment-input")) ? (10) : (100));
          console.log(value + " < " + 10 + " is " + ((value < ($(field).hasClass("assignment-input")) ? (10) : (100))));
          $(field).val((parseFloat($(field).val()) + 0.1).toFixed(1));
        }
      }
    }
    // Else decrease the value
    else if ($(this).find(".glyphicon-chevron-down").size() > 0)
    {
      if (validateFields(field, 0, ($(field).hasClass("assignment-input")) ? (10) : (100)))
      {
       // Stop from decreasing over the lower bound
        if (0 < parseFloat(value))
        {
          $(field).val((parseFloat($(field).val()) - 0.1).toFixed(1));
        }
      }
    }
  });
});